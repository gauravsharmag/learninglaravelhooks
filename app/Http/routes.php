<?php

/*
|--------------------------------------------------------------------------
| Application Routes  
|--------------------------------------------------------------------------
| 
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/new_laravel', function () {         
	echo "HELLO WORLD TESTING.ggg..999"; 
    return view('welcome');      
});

Route::get('/new_laravel/test',function() {           
	echo "Testing..."; 
});

Route::get('/new_laravel/trigger_hook','HookController@triggerHook');	    
Route::post('/new_laravel/trigger_hook','HookController@triggerHook');   

