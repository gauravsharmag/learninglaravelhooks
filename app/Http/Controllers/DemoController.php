<?php
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;  

class DemoController extends Controller {     
	
	public function demo() {
		echo "Demo";
	}

	public function test() {
		echo "TEST";
	}

	public function my() {  
		
	}
	public function you() {   
		
	}	

	public function newDemo() {
			echo "newDemo";
	}

}